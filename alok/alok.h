#ifndef ALOK_H_
#define ALOK_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <stdbool.h>

#define PERMANENT 0b1

// TODO: These macros needs to account for the return value of "alok"
// Generic allocations
// BUG: sizeof(ty) is wrong here, consider the fact if we want
#define allocate(ty, name) ty* name; alok(handler, sizeof(ty), __func__, (void*)&name, 0);
#define allocate_scoped(ty, name, scope) ty* name; alok(handler, sizeof(ty), scope, (void*)&name, 0);
#define allocate_flags(ty, name, flags) ty* name; alok(handler, sizeof(ty), __func__, (void*)&name, flags);
#define allocate_scoped_flags(ty, name, flags, scope) ty* name; alok(handler, sizeof(ty), scope, (void*)&name, flags);

// Permanent allocations
#define allocate_permanent(ty, name) ty* name; alok(handler, sizeof(ty), __func__, (void*)&name, PERMANENT);
#define allocate_scoped_permanent(ty, name, scope) ty* name; alok(handler, sizeof(ty), scope, (void*)&name, PERMANENT);
#define allocate_count_permanent(ty, name, count) ty* name; alok(handler, sizeof(ty) * count, __func__, (void*)&name, PERMANENT);
#define allocate_count_scoped_permanent(ty, name, count, scope) ty* name; alok(handler, sizeof(ty) * count, scope, (void*)&name, PERMANENT);

// The most basic wrapper around `alok`.
#define allocate_count_scoped_flags(ty, name, count, scope, flags) ty* name; alok(handler, sizeof(ty) * count, scope, (void*)&name, flags)

// Freeing
#define afree(allocation) alok_free(handler, allocation);
#define afree_scope(scope) alok_free_scope(handler, scope);
#define afree_func() alok_free(handler, __func__);

struct AlokAllocation {
    int scope; // Hash of the name of the scope
    int flags; // Flags applying to this specific allocation

    size_t allocation_size; // How big the allocation behind self.ptr is
    void* ptr; // We lose the type for the ptr :(

    LIST_ENTRY(AlokAllocation) allocd;
};

struct AlokHandler {
    // A list of all allocations.
    LIST_HEAD(AllocationsHead, AlokAllocation) allocations;

    // How many allocations are currently active in this handler.
    int n_allocations;

    // How many bytes are currently allocated in the handler.
    // Does not account for the `AlokAllocation` structs mallocd during alok.
    // Maybe we should account for ALL mallocd things?
    // That would be more appropriate considering the allocation limit
    size_t allocated_bytes;

    // The maximum amount of bytes allowed to be allocated. (WIP)
    size_t allocation_limit;
};

void change_allocation_scope(struct AlokAllocation* alloc, const char* new_scope);

void change_allocation_flags(struct AlokAllocation* alloc, int flags);

int alok(struct AlokHandler* self, size_t count, const char* scope, void** destination_ptr, int alok_flags);

int alok_free(struct AlokHandler* self, void* ptr);

int alok_free_scope(struct AlokHandler* self, const char* scope);

struct AlokAllocation* get_allocation(struct AlokHandler* self, void* needle);

#endif
