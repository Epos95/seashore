#include "../include/eaux.h"
#include "../include/alok.h"
#include "alok.h"
#include <string.h>
#include "second_file.h"

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

extern struct AlokHandler* handler;
void print_ptr_info(char* ptr) {
    // Can we assume the handler is initialized?
    if (handler->n_allocations == 0) {
        // No
        ERROR("Handler has no allocations ;_;");
    } else {
        // Yes
        SUCCESS("Handler had %d allocations :D", handler->n_allocations);
    }

    struct AlokAllocation* np;
    bool found_thing = false;
    LIST_FOREACH(np, &handler->allocations, allocd) {
        char* val = (char*)(np->ptr);
        INFO("allocation had value: %s (%p)", val, np->ptr);
        DEBUG("%s == %s", ptr, val);
        for (int i = 0; i < 3; i++) {
            DEBUG("[%d] %c == %c", i, ptr[i], val[i]);
        }

        if (val && strncmp(val, ptr, 2)) {
            found_thing = true;

        }
    }

    if (!found_thing) {
        ERROR("Did not find allocated value!");
    } else {
        SUCCESS("Found the allocated value!");
    }
}
