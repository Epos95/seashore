#include "alok.h"
#include "../include/eaux.h"
#include "second_file.h"

extern struct AlokHandler* handler;
int main(int argc, char* argv[]) {

    handler = malloc(sizeof(struct AlokHandler));

    allocate_scoped(void, dest, "pog");
    allocate_flags(void, second, PERMANENT);

    allocate(char, str);
    str = "wow";
    print_ptr_info(str);

    allocate(char*, str2);
    str2[0] = "wow";
    INFO("str2[0] = %c", str2[0][1]);

    int* real;
    alok(handler, sizeof(int), "yeet", (void *)&real, 0);
    *real = 69;

    int prev = handler->n_allocations;
    alok_free_scope(handler, __func__);
    SUCCESS("freed %d allocations in scope \"%s\"", prev - handler->n_allocations, __func__ );

    struct AlokAllocation* np;
    prev = handler->n_allocations;
    bool found_thing = false;
    LIST_FOREACH(np, &handler->allocations, allocd) {
        DEBUG("allocation had value: %d (%p)", *((int *)np->ptr), np->ptr);
        if (*real == *((int *)np->ptr)){
            SUCCESS("Allocated value %d found!", *real);
            found_thing = true;
        }

        alok_free(handler, np->ptr);
    }

    if (!found_thing) {
        ERROR("Did not find allocated value!");
    }

    INFO("Finding allocation!");
    struct AlokAllocation *alloc = get_allocation(handler, second);
    change_allocation_flags(alloc, 0);

    int r;
    if (IS_ERR(r = alok_free(handler, second))) {
        DEBUG("r=%d", r);
        ERROR("Failed to deallocate second pointer");
    } else {
        SUCCESS("Freed %d allocations with alok_free", r);
    }

    if (handler->n_allocations == 0) {
        SUCCESS("Everything is freed!");
    } else {
        ERROR("%d allocations remaining...", handler->n_allocations);
    }

    free(handler);
}
