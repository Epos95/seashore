#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/queue.h>
#include <time.h>

#include "../include/eaux.h"
#include "alok.h"

struct AlokHandler* handler;

// Djb2 hashing algorithm
// See http://www.cse.yorku.ca/~oz/hash.html for more info
static int hash(const char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;

    return hash;
}

void change_allocation_scope(struct AlokAllocation* alloc, const char* new_scope) {
    if (!alloc) {
        INFO("Tried to dereference null pointer...");
        return;
    }
    alloc->scope = hash(new_scope);
}

void change_allocation_flags(struct AlokAllocation* alloc, int flags) {
    if (!alloc) {
        INFO("Tried to dereference null pointer...");
        return;
    }
    alloc->flags = flags;
}

// It would be fun to introduce a automatic warning for going over a certain amount of memory aswell...
//
// What we cant handle then are structs with nested references (e.g making a reference live longer )
// I also assume we cant handle threads very well either...
int alok(struct AlokHandler* self, size_t count, const char* scope, void** destination_ptr, int alok_flags) {

    // See note for AlokHandler.allocated_bytes
    if (self->allocation_limit && self->allocated_bytes + count > self->allocation_limit) {
        // I had the cursed idea of allocating things to *files* instead of to memory...
        // This would only work in a language like rust tho where we can write our own
        // dereferencing functions, checking wether a deref belongs to actual RAM or to a file...
        ERROR("Allocation would surpass the allocation limit of %d, preventing", self->allocation_limit);

        return -1;
    }

    int hashed_scope = hash(scope);
    INFO("%s was hashed to %d", scope, hashed_scope);
    *destination_ptr = malloc(count);

    // Create struct to represent the allocation
    struct AlokAllocation* allocated = malloc(sizeof(struct AlokAllocation));
    allocated->flags = alok_flags;
    allocated->scope = hashed_scope;
    allocated->ptr = *destination_ptr;
    allocated->allocation_size = count;

    LIST_INSERT_HEAD(&self->allocations, allocated, allocd);
    self->n_allocations++;
    self->allocated_bytes += count;

    return 0;
}

// Deallocates the given `AlokAllocation`
static void __deallocate(struct AlokHandler* self, struct AlokAllocation* allocation) {
    // If the permanent flag is set for the allocation, we skip it
    // TODO: We gotta make sure to skip it for ALL allocations pointing at that pointer tho...
    //       Or permanent gets set for all allocations pointing at it...
    if (!allocation) {
        INFO("Tried to dereference null pointer...");
        return;
    }

    DEBUG("Starting to free");
    if (!(allocation->flags & PERMANENT)) {
        self->n_allocations--;
        self->allocated_bytes -= allocation->allocation_size;

        allocation->ptr = NULL;
        free(allocation->ptr);
        LIST_REMOVE(allocation, allocd);
        allocation = NULL;
        free(allocation);
    }

}

int alok_free(struct AlokHandler* self, void* ptr) {
    struct AlokAllocation* allocation;
    int r = 0;
    LIST_FOREACH(allocation, &self->allocations, allocd) {
        if (allocation->ptr == ptr) {
            __deallocate(self, allocation);
            r++;
        }
    }

    return r == 0 ? -1 : r;
}

// TODO: Return a proper error value? free cant error, technically...
int alok_free_scope(struct AlokHandler* self, const char* scope) {
    int hashed_scope = hash(scope);

    struct AlokAllocation* allocation;
    LIST_FOREACH(allocation, &self->allocations, allocd) {
        // TODO: Allow for "inheriting" scopes, so if we have a scoped allocation inside a function,
        //       the scoped allocation needs to be free'd alongside the functions scope... maybe?
        //       Depends on how its used in practice
        if (allocation->scope == hashed_scope) {
            __deallocate(self, allocation);
        }
    }

    return 0;
}

struct AlokAllocation* get_allocation(struct AlokHandler* self, void* needle) {
    struct AlokAllocation* allocation;
    LIST_FOREACH(allocation, &self->allocations, allocd) {
        DEBUG("allocation->ptr = %p", allocation->ptr);
        DEBUG("needle          = %p", needle);
        if (allocation->ptr == needle) {
            return allocation;
        }
    }

    return NULL;
}
