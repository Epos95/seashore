#include <string.h>

int xor_crypt(char* buffer, char* secret) {
    int secret_len = strlen(secret);
    int buffer_len = strlen(buffer);

    int secret_index = 0;
    for (int i = 0; i < buffer_len; i++) {
        buffer[i] ^= (secret[secret_index] & 0xff);
        secret_index++;
        if (secret_index == secret_len) {
            secret_index = 0;
        }
    }
    return 0;
}
