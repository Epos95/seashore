use core::ffi::c_void;
use nix::sys::mman::ProtFlags;
use std::ptr::NonNull;

pub trait EncryptedDispatch {
    fn encrypt(&self) -> Option<()>;
    fn decrypt(&self) -> Option<()>;
    fn do_thing(&self);
    fn make_addr_writeable(address: *mut c_void) -> Result<(), ()> {
        let aligned = align(address);
        let ptr = NonNull::new(aligned).unwrap();

        // TODO: Improve this statement to make error handling easier
        if address >= aligned && address <= aligned.wrapping_add(PAGE_SIZE) && aligned.is_aligned()
        {
            // We are aligned and in page

            // We can read /proc/pid/maps to find the original flags.
            let flags = ProtFlags::PROT_WRITE | ProtFlags::PROT_READ | ProtFlags::PROT_EXEC;
            unsafe {
                nix::sys::mman::mprotect(ptr, PAGE_SIZE, flags).unwrap();
            }

            Ok(())
        } else {
            Err(())
        }
    }

    fn length_of_function() -> Option<usize> {}
}

struct Dispatcher;

const PAGE_SIZE: usize = 4096;
fn align(address: *mut c_void) -> *mut c_void {
    let address = address as usize;
    ((address) & !(PAGE_SIZE - 1)) as *mut c_void
}

impl EncryptedDispatch for Dispatcher {
    // Encrypt a function during run time,
    // Write another method for encrypting after compilation (before deployment)
    // and a macro for encryption during compile time (kinda)
    fn encrypt(&self) -> Option<()> {
        // Find address of self.do_thing (will it behave the same as normally?)
        let mut addr = Self::do_thing as *mut c_void;

        Self::make_addr_writeable(addr).unwrap();

        unsafe {
            let mut fn_length: Option<usize> = None;
            // TODO: This search should be smarter... should also be page aware
            for i in 3..100 {
                let byte = (addr as *mut u8).offset(i).read();
                if byte & 0xff == 0xc3 {
                    fn_length = Some(i as usize);
                    break;
                }
            }

            let fn_length = fn_length.expect("Function too long (or ret instruction not found)");
            dbg!(fn_length);
            if !(addr as *mut u8).is_aligned() {
                panic!("Not aligned!");
            }

            let mut function_contents = vec![0u8; fn_length];
            (addr as *const u8).copy_to(function_contents.as_mut_ptr(), fn_length);
            let password = String::from("Password!");
            let password_iterator = password.chars().cycle();
            let mut contents: Vec<u8> = function_contents
                .iter_mut()
                .zip(password_iterator)
                .map(|(mut x, p)| *x ^ (p as u8))
                .collect();
            (addr as *mut u8).copy_from(contents.as_mut_ptr(), fn_length);
        };

        Some(())
    }

    fn decrypt(&self) -> Option<()> {
        // Do encrypt but write with decrypted data instead
        todo!()
    }

    #[no_mangle]
    fn do_thing(&self) {
        println!("Hello friend!");
    }
}

use proc_macro2::TokenStream;

// We just need to test if a proc macro can add all function addresses in a scope to a single struct...
// What we have to do is just append each detected function name to a file
// The dispatcher can then use the unstringify macro to store the address of each function, by name
//
#[proc_macro_attribute]
pub fn annotate(input: TokenStream, annotated_item: TokenStream) -> TokenStream {
    println!("{annotated_item}");
    annotated_item
}

#[annotate]
fn testingggg() {}

fn main() {
    let d = Dispatcher;
    d.encrypt();
    //d.decrypt();
    d.do_thing();
}
