#include <elf.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/eaux.h"
#include "../include/alok.h"
#include "lib_encrypt.h"

// Linear search for the given section type in the section header table, return the index if found
// Otherwise return -1.
int _section_idx(int section_type, Elf64_Shdr* sh_table, int table_size) {
    for (int i = 0; i < table_size; i++) {
        if (sh_table[i].sh_type == section_type) return i;
    }

    return -1;
}

// Take file, name to search for as a argument
// Return the offset into the file at which the function is
int find_function_offset(FILE* file, char* needle, Elf64_Sym* symbol_entry) {
    int ret = -1;
    // Assume file to be valid

    // Read the ELF header.
    INFO("Reading elf header from given file");
    Elf64_Ehdr header;
    int r = fread(&header, sizeof(Elf64_Ehdr), 1, file) ;
    if(r == 0) {
        ERROR("Failed to read header due to %s", strerror(errno));
        return ret;
    } else {
        SUCCESS("Read header!");
    }


    // Read the section header table.
    // See https://www.wikiwand.com/en/Executable_and_Linkable_Format#Section_header
    INFO("Reading section header table");
    int table_size = header.e_shentsize * header.e_shnum;
    Elf64_Shdr* sh_table = malloc(table_size);
    fseek(file, header.e_shoff, SEEK_SET);
    fread(sh_table, 1, table_size, file);

    // We need to read the dynsym section to access the symbols in the symbol table, find it in the section header
    int symtab_index = _section_idx(SHT_SYMTAB, sh_table, header.e_shnum);
    if (symtab_index == -1) {
        ERROR("Failed to find SHT_SYMTAB");
        goto EXIT_EARLY;
    } else {
        SUCCESS("Found symbol table at index %d!", symtab_index);
    }
    Elf64_Shdr dynsym_section = sh_table[symtab_index];
    if (dynsym_section.sh_size % dynsym_section.sh_entsize != 0) {
        ERROR("Corrupted ELF file!");
        goto EXIT_EARLY;
    }
    int n_symbols = (int)(dynsym_section.sh_size / dynsym_section.sh_entsize);
    DEBUG("Found %d symbols", n_symbols);

    // Read the symbol table so we can access function names!
    int offset = dynsym_section.sh_offset;
	Elf64_Sym* symbol_table = malloc(sizeof(Elf64_Sym) * n_symbols);
    fseek(file, offset, SEEK_SET);
	fread(symbol_table, sizeof(Elf64_Sym), n_symbols, file);

	INFO("Iterating over sections to find location of \"%s\"", needle);
	for (int section_idx = 0; section_idx < header.e_shnum; section_idx++) {
        Elf64_Shdr section_header = sh_table[section_idx];
        DEBUG("Working on section index: %d, type is: %d\t%d < %d", section_idx, section_header.sh_type, section_idx, header.e_shnum);
        if (section_header.sh_type != SHT_STRTAB) continue;

        // Read the strtab section to a byte buffer so we can search through it.
        char* buffer = malloc(sizeof(char) * section_header.sh_size);
        fseek(file, section_header.sh_offset, SEEK_SET);
        fread(buffer, sizeof(char), section_header.sh_size, file);

        // For every entry in the symbol table check if it is what we are looking for.
        for (int entry = 0; entry < n_symbols; entry++) {
            int name_index = symbol_table[entry].st_name;

            char* res = malloc(256);

            // If memccpy fails it returns a null pointer
            if (!memccpy(res, buffer + name_index, 0, 256)) {
                ERROR("memccpy failed, see docs (it could also be that we overflowed the buffer)! (res = %s)", res);
                free(res);
                free(buffer);
                goto EXIT_EARLY;
            }

            if (strcmp(res, needle) == 0) {
                SUCCESS("Found \"%s\"", needle);
                memcpy(symbol_entry, &symbol_table[entry], sizeof(Elf64_Sym));
                ret = 0;
                free(res);
                free(buffer);
                goto EXIT_EARLY;
            }
            free(res);
        }
        free(buffer);
    }

EXIT_EARLY:
    free(sh_table);
    free(symbol_table);

    return ret;
}

// Should take the file to encrypt as the argument
// Remaining arguments should be the functions to encrypt inside the binary
int main(int argc, char** argv) {
    if (argc == 1) {
        ERROR("Requires atleast one argument!");
        return -1;
    }

    char* filename = argv[1];
    FILE* file = fopen(filename, "r+");
    if (!file) {
        ERROR("Failed to open file \"%s\"! (errno: %s)", filename, strerror(errno));
        fclose(file);
        return -1;
    } else {
        SUCCESS("Succesfully opened %s", filename);
    }

    char* needle = "other_function";
    Elf64_Sym sym;
    int offset = find_function_offset(file, needle, &sym);
    if IS_ERR(offset) {
        ERROR("Failed to find %s in file!", needle);
        fclose(file);
        return -1;
    } else {
        SUCCESS("Found %s in file!", needle);
    }

    DEBUG("offset: 0x%x\tsize: 0x%x (%d)", sym.st_value, sym.st_size, (int)sym.st_size);
    int symbol_size = (int)sym.st_size - 1;
    char* contents = malloc(symbol_size);
    fseek(file, sym.st_value & 0xffff, SEEK_SET);
    fread(contents, sizeof(char), symbol_size, file);

    xor_crypt(contents, "password!");
    SUCCESS("Encrypted contents with length %d!", symbol_size);
    fseek(file, sym.st_value & 0xffff, SEEK_SET);
    if(fwrite(contents, sizeof(char), symbol_size, file) != symbol_size) {
        ERROR("Failed writing everything to file!");
    }

    fclose(file);
    SUCCESS("Wrote encrypted function to file");

    return 0;
}
