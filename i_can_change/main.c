#include <errno.h>
#include "../include/eaux.h"
#include "lib_encrypt.h"
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>


void another_one() {}

int function2(int val, int sec) {
    return 5 + val;
}

int other_function(int val) {
    __asm__ volatile("nop");
    __asm__ volatile("nop");
    __asm__ volatile("nop");

    return 5 + val;
}

int find_pivot(char* instructions) {
    int ctr = 0;
    int offset = 0;

    for(char* ins = instructions; (*ins & 0xff) != 0xc3; ins++) {
        offset++;
        if ((*ins & 0xff) == 0x90) ctr++;
        if (ctr == 3) return offset;
    }

    return -1;
}

int main() {
    char* f_addr = (char*)&other_function;
    INFO("Found address of function @ %p", f_addr);

    char* page = (char* )((long long int)f_addr & ~(getpagesize()-1));
    int r = mprotect(page, getpagesize(), PROT_WRITE | PROT_READ | PROT_EXEC);
    if IS_ERR(r) {
        ERROR("Failed to mprotect due to: %s", strerror(errno));
        return -1;
    }
    SUCCESS("Changed protections of self!");

    INFO("Starting linear search to find function length!");
    // This is very hacky!
    // In practice we should overwrite the encrypted ret with a known sentinel value
    // which can be replaced with ret (0xc3) when the contents are decrypted!
    int function_length = 0;
    for (int i = 0; i < 100; i++) {
        DEBUG("f_addr[%d] & 0xff = %x", i, f_addr[i] & 0xff);

        // I bet this is due to the encryption scheme that this fails when the
        // function is a different length (e.g compiled on different architecture)
        if ((f_addr[i] & 0xff) == 0xc3) {
            //i++;
            SUCCESS("Found end of function @ %d!", i);
            function_length = i;
            break;
        }
    }

    if (function_length == 0) {
        ERROR("Failed to find length of the function!");
        return -1;
    } else {
        INFO("function has length %d", function_length);
    }


    char* buffer = malloc(function_length);
    // TODO: Error check me!
    memcpy(buffer, f_addr, function_length);

    xor_crypt(buffer, "password!");

    // TODO: Error check me too!
    memcpy(f_addr, buffer, function_length);

    SUCCESS("other_function(5): %d", other_function(5));
}
