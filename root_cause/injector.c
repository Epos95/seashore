// Injects the latest version of the binfmt

#include "../include/eaux.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <errno.h>

// The thing to replace the magic ELF number
const char MAGIC[4] = {0x45, 0x50, 0x4f, 0x53}; // "EPOS"

// Path to the virtual file to register binfmts
const char* BINFMT_REGISTER = "/proc/sys/fs/binfmt_misc/register";

// Path to our own binfmt so we can remove it
const char* OUR_BINFMT = "/proc/sys/fs/binfmt_misc/root_cause";

bool is_sudo() {
    return geteuid() == 0;
}

int inject(const char* arg) {
    int ret = 0;

    FILE* fp = fopen(arg, "rb+");
    if (fp == NULL) {
        ERROR("fopen failed! error: %s", strerror(errno));
        ret = -1;
        goto exit;
    } else {
        INFO("Succesfully opened %s", arg);
    }

    // Get the file size, rewind the fp aswell
    fseek(fp, 0, SEEK_END);
    int file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buf = malloc(file_size);
    int n_read = fread(buf, sizeof(char), file_size, fp);
    fseek(fp, 0, SEEK_SET);

    if (n_read != file_size) {
        ERROR("Read %d bytes, wanted to read %d bytes.", n_read, file_size);
        ret = -1;
        goto exit;
    } else {
        INFO("Read %d bytes!", n_read);
    }

    memcpy(buf, MAGIC, 4);
    int n_written = fwrite(buf, sizeof(char), file_size, fp);

    if (n_written != file_size) {
        ERROR("Wrote %d bytes, wanted to write %d bytes. error: %s", n_written, file_size, strerror(errno));
        ret = -1;
        goto exit;
    } else {
        SUCCESS("Finished editing %s", arg);
    }

exit:
    free(buf);
    fclose(fp);

    return ret;
}

int insert_binfmt() {
    INFO("Inserting binfmt!");
    if (!is_sudo()) {
        ERROR("This command requires sudo!");
        return -1;
    }

    // Actually craft this dyanmically... lol
    char* binfmt = "-root_cause-M-0-EPOS--/home/max/Code/seashore/root_cause/interpreter-";

    int limit = strlen(binfmt) + strlen("echo >") + strlen(BINFMT_REGISTER) + 12;
    char* buffer = malloc(limit);
    int cur_len = snprintf(buffer, limit, "echo %s > %s", binfmt, BINFMT_REGISTER);
    int ret = system(buffer);

    INFO("ret is %d");

    // TODO: Maybe check if binfmt_misc is mounted

    // Use system to echo the string into binfmt_misc



    free(buffer);
    return 0;
}

int remove_binfmt() {
    INFO("Removing binfmt!");

    // Check if OUR_BINFMT even exists before doing anything
    FILE* fp = fopen(OUR_BINFMT, "r");
    if (!fp) {
        INFO("File does not exist!");
        return 0;
    }

    if (!is_sudo()) {
        ERROR("This command requires sudo!");
        return -1;
    }

    int limit = strlen(OUR_BINFMT) + strlen("echo -1 >") + 5;
    char* buffer = malloc(limit);
    snprintf(buffer, limit, "echo -1 > %s", OUR_BINFMT);
    int ret = system(buffer);

    return 0;
}

int main(int argc, const char* argv[]) {
    // Make sure we have a arg to operate on
    if (argc < 2) {
        ERROR("This program requires atleast one argument!");
        return -1;
    }

    int ret;
    if (strcmp("insert_binfmt", argv[1]) == 0) {
        ret = insert_binfmt();
    } else if (strcmp("remove_binfmt", argv[1]) == 0){
        ret = remove_binfmt();
    } else if (strcmp("inject", argv[1]) == 0) {
        if (argc < 3) {
            ERROR("The inject command requires a argument as a file to operate on.");
            return -1;
        }

        ret = inject(argv[2]);
    } else {
        ERROR("Unrecognized command: \"%s\"", argv[1]);
        return -1;
    }

    if (ret < 0) {
        ERROR("%s failed!", argv[1]);
    } else {
        SUCCESS("%s succeeded!", argv[1]);
    }


    // Do a sudo check!

    return 0;
}
