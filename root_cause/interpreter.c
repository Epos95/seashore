
#include "../include/eaux.h"
#include <stdio.h>

int main(int argc, const char* argv[]) {
    SUCCESS("Interpreter called with args:");
    for (int i = 0; i < argc; i++) {
        printf("\t%s\n", argv[i]);
    }

    // Spawn process
    // Do things with that process
    // Like... manipulate the output (and maybe even input)

    return 0;
}
