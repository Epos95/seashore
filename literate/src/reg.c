#include <stdbool.h>
#include "../../include/eaux.h"

#define RAX 0b0000
#define RCX 0b0001
#define RDX 0b0010
#define RBX 0b0011
#define RSP 0b0100
#define RBP 0b0101
#define RSI 0b0110
#define RDI 0b0111
#define R8  0b1000
#define R9  0b1001
#define R10 0b1010
#define R11 0b1011
#define R12 0b1100
#define R13 0b1101
#define R14 0b1110
#define R15 0b1111

// Struct responsible for all interactions with registers.
//
//
struct RegWarden {
    // Or maybe a Register struct instead?
    int registers[16];
    bool in_use[16];
    // Relative or absolute memory? I think we do it relative to the rbp...
    long memory_location[16];
};

// Gives a register which is clear to use, should be able to write to the
// codebuffer for when we need to move a register into memory
int get_register(struct RegWarden* w, char* code_buffer) {
    // Find a empty register and return it 
    for (int i = 0; i < 16; i++) {
	if (!w->in_use[i]) {
	    w->in_use[i] = true;
	    return w->registers[i];
	}
    }

    ERROR("No unused registers found!");
    // TODO: If there are no valid registers try to get one from memory
    return -1;
}

// When calling a function we should first save all the registers
// which are callee saved
// Marke the non-callee saved ones as unused.
int save_registers(struct RegWarden* w, char* code_buffer, int cb_ptr) {
    // TODO: Maybe export this list for use in other places too
    int callee_saved[] = {
	RBX,
	RSP,
	RBP,
	R12,
	R13,
	R14,
	R15,
    };

    for (int i = 0; i < 7; i++) {
	int cur_reg = callee_saved[i];
	// Push stuff!
	// TODO: Needs the library i wrote on windows for writing instructions
	code_buffer[cb_ptr++] = ??;
    }


    return cb_ptr;
}

void load_registers(struct RegWarden* w, char* code_buffer) {
    // Load all callee saved regi
}
