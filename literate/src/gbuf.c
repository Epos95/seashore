#include <math.h>
#include <stdlib.h>

#include "../include/gbuf.h"
 
// Maybe allow for more options?
void init_buffer(struct GrowableBuffer *buf, union GrowthAmount ga, enum GrowthStrategy gs) {
    buf->cur_size = 0;
    buf->max_size = 1024;
    buf->ptr = malloc(1024);
    buf->ga = ga;
    buf->gs = gs;
}

void grow_buffer(struct GrowableBuffer *buf) {
    int new_max_size;
    switch (buf->gs) {
        case GS_EXP:
            new_max_size = pow(buf->max_size, buf->ga.exp);
            break;

        case GS_LIN:
            new_max_size = buf->max_size + buf->ga.lin;
            break;
        
        default:
            // Wut?
            break;
    }
    
    buf->max_size = new_max_size;
    realloc(buf->ptr, new_max_size);
}

void insert_char(struct GrowableBuffer *buf, char c) {
    if (buf->cur_size + 1 >= buf->max_size) {
        grow_buffer(buf);
    }

    buf->ptr[buf->cur_size] = c;
    buf->cur_size++;
}

void insert(struct GrowableBuffer *buf, char* c, int size) {
    for (int i = 0; i < size; i++) {
        insert_char(buf, c[i]);
    }
}
