#include "../../include/eaux.h"

#include "../include/lexer.h"
#include "../include/codegen.h"
#include "../include/x86_codegen.h"
#include "../include/gbuf.h"

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>


struct State {
    int cur_rbp_offset;
    int cur_scope;

    // TODO: Maybe replace with gbuf
    char* code_buffer;
    int cb_len;
    int cb_ptr;
};

void write_byte(struct State* s, unsigned char byte) {
    // TODO: Replace with gbuf write
    s->code_buffer[s->cb_ptr++] = byte;
}

void write_bytes(struct State* s, unsigned char* bytes, int n) {
}



int define_variable(char name[32],
		    size_t size,
		    int cur_rbp_offset,
		    int scope,
		    struct Variable** chain) {
    struct Variable* var;
    if (*chain == NULL) {
	INFO("Not chain");
	*chain = malloc(sizeof(struct Variable));
	var = *chain;
    } else {
	INFO("Chain exists");
	struct Variable* last = *chain;
	while (last->next != NULL) {
	    last = last->next;
	}

	last->next = var;
    }


    memcpy(var->name, name, 32);
    var->scope = scope;
    var->size = size;

    // We offset the rbp pointer more to make space
    // for the variable on the stack
    int new_rbp_offset = cur_rbp_offset + size;
    var->rbp_offset = new_rbp_offset;
    var->next = NULL;
    
    // That is now the new rbp offset
    return new_rbp_offset;
}

// Gets the RBP offset of the given variable name `n`.
// This is equivalent to a lookup since we can use the offset
// in the binary code to access the variable
int get_offset(struct Variable* chain, char* n) {
    // We need to get the variable with the matching name and the LOWEST scope
    int highest_scope = 0;
    int to_return = -1;
    struct Variable* next = chain;
    while (next != NULL) {
	bool name = strncmp(next->name, n, 32) == 0;
	if (next->scope >= highest_scope && name) {
	    highest_scope = next->scope;
	    // I dont think this will work...
	    to_return = next->rbp_offset;
	}

	next = next->next;
    }

    return to_return;
}

// Generate the binary code for a specific expression
void generate_for_expression(
    struct GrowableBuffer* gbuf,
    struct Variable* variables,
    struct Expression expr,
    struct State* state
) {

    struct Word* current = expr.syntax_chain;
    SUCCESS("Starting work on expr: %s (%p)", expr.raw, expr.syntax_chain);
    int chain_len = 0;
    while (current != NULL) {
	INFO("[%d] %s", chain_len, current->data);
	chain_len++;
	current = current->next;
    }
    current = expr.syntax_chain;
    DEBUG("%p", expr.syntax_chain);

    DEBUG("Generating for word: %d", current->wt);


    // One expression might result in multiple instructions tho??
    // Maybe we generate one instruction at a time?
    struct FullInstruction* fi = malloc(sizeof(struct FullInstruction));

    // *define* a new variable, we need a different thing for assigning to a existing variable
    if (expr.declaration_flags & F_TYPE) {
	state->cur_rbp_offset = define_variable(
	    expr.name, sizeof(int),
	    state->cur_rbp_offset, state->cur_scope,
	    &variables);
	SUCCESS("Allocated a new variable: \"%s\"", expr.name);

	// TODO: Actually write binary code for putting the variable into
	//       memory (after it has a value) (pretty much the rest of this function)
    }

    // TODO: Function calls need to be isolated into temporary variables
    //       so we can use their results later!

    // Do we maybe convert the expression into reverse polish notation?

    
    // So for a function call we need to:
    // Create a new variable on the stack (maybe...?) Or,
    // we just need a open register to store return value in.
    // Call the function, returning to that variable (or register)
    // Use that variable or function call!


    // Do we maybe convert the expression into reverse polish notation?
    // I honestly think so...
    // yes, but also this is a consequence of how badly we classify expressions
    // we need to optimize how we handle expressions...

    

    int w_idx = 0;
    while (current != NULL) {
	if (current->wt == VARIABLE) { 
	    // Maybe dont handle this case at all, accessing a variable only matters when its
	    // in a function or operator and stuff

	    int variable_rbp_offset = get_offset(variables, current->data);
	    // After we have the offset we have to load the thing INTO some
	    // register i guess...
	    // Then use that register in our calculation!
	} else if (current->wt == FUNCTION) {
	    INFO("Calling: \"%s\"", current->data);
	    // How do we handle the function args??
	    // We need ANOTHER codegen function call for that ;_;
	} else if (current->wt == OPERATOR) {
	    // TODO: Use a instruction struct here for building the instruction step by step

	    switch (current->data[0]) {
	    case '+':
		break;
	    default:
		ERROR("Unrecognized data: %s", current->data);
		break;
	    }

	    if (current->data[0] == '+') {

	    }

	    // Grab the previous and next words.
	    // Apply this function 


	} else if (current->wt == IMMEDIATE) {
	    // Easy case of putting a immediate on the stack
	    if (chain_len == 1 && expr.declaration_flags == F_TYPE) {
		// Predetermined size of 16 for buffering reasons!
		unsigned char ins[16] = {
		    0xc7, // Op code for the kind of mov we want to do
		    0x43, // rm byte for writing RBX (0b011) at a offset
		    0xfc, // -4 in 1s complement (252)
		};

		int number = atoi(current->data);

		// Write each byte in the immediate into the ins buffer
		// while saving space for the 3 already written bytes.
		for (int i = 0; i < sizeof(int); i++) {
		    ins[i + 3] = (number >> i * 8) & 0xff;
		}


		INFO("generating instructions:");
		for (int i = 0; i < 3 + sizeof(int); i++) {
		    printf("\t0x%x\n", ins[i] & 0xff);
		}

		insert(gbuf, (char*)ins, 3 + sizeof(int));
	    } else if (expr.declaration_flags == F_TYPE) {
		// A statement like:
		// int x = 4 + x;
		
		// Allocate variable based on another value on the stack or heap
		// Allcoate the variable space THEN execute the rest of the expression!
		// This lets us use the declared variable space in our execution :D
	    } else {
		INFO("Found a random immediate i think?");
	    }

	    // Find a free register (maybe from state?)
	    // Put immediate in that register!
	    asm("nop");
	}

        current = current->next;
    }
}


void push_args(char* code_buffer,
	       struct ArgList* arg_list,
	       struct Variable** chain
    ) {

    // How many registers are in use right now.
    int in_use = 0;
    // See: https://wiki.osdev.org/X86-64_Instruction_Encoding#Registers
    // for info about the encoding (should also be abstracted in the future)
    int registers[] = {
	0b0111, // rdi
	0b0110, // rsi
	0b0010, // rdx
	0b0001, // rcx
	0b1000, // r8
	0b1001  // r9
    }; 
    

    struct ArgList* current = arg_list;
    while (current != NULL) {
	int rbp_offset = get_offset(*chain, current->name);
	if (rbp_offset == -1) {
	    ERROR("Undeclared variable: %s", current->name);
	}

	if (in_use > 5) {
	    ERROR("Too many arguments! We need to push to stack");
	    break;
	}
	int reg = registers[in_use++];
	// Take whatever is at the rbp offset and move it into reg 
	// i think we want to use LEA for this (or mov?)
	// It should look something like: LEA | MOV $RSI, [RBX + offset] (i think)

	current = current->next;
    }
}

int codegen_function(char* code_buffer, struct Function* function, struct Variable** chain) {
    int buffer_pointer = 0;

    struct GrowableBuffer* gbuf = malloc(sizeof(struct GrowableBuffer));
    union GrowthAmount ga = { .lin = 64 };
    init_buffer(gbuf, ga, GS_LIN);

    struct State state = {
	// Everytime we do something with the RBP we have to decrement the stack pointer aswell
	.cur_rbp_offset = 0,
	.cur_scope = 1 // We are in a function, therefore 
	               // we use a once nested scope! (the 0th scope is all scopes beneath us!)
    };

    // TODO: Handle the functions arguments
    // Due to how literate will be called, this is not necessary for the "entrypoint"
    // since the entrypoint will have a predefined signature which never changes, therefore
    // the arg setup will never change either
    // We will need it for all other functions besides entrypoint tho.
    push_args(code_buffer, function->args, chain);

    // The function prologue (We are in the function from here on)
    unsigned char prelude[] = { 0x55, 0x48, 0x89, 0xe5 }; 
    insert(gbuf, (char*)prelude, 4);
    
    // TODO: "unpack" the arguments into registers!

    DEBUG("Handling %d expressions", function->n_expressions);
    
    // TODO: handle each expression in the function with generate_for_expression
    for (int i = 0; i < function->n_expressions; i++) {
	INFO("Parsing new expression!");
	struct Expression expr = function->expressions[i];
	generate_for_expression(gbuf, *chain, expr, &state);
    }

    // Function exit
    insert_char(gbuf, 0x5d);
    insert_char(gbuf, 0xc3);

    memcpy(code_buffer, gbuf->ptr, gbuf->cur_size);

    return gbuf->cur_size;
}
