#include "../../include/eaux.h"

#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#define max(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b;       \
})

#define min(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b;       \
})

struct LexedFunction {
    int n_args;
    char args[32][64];
    char name[32];
};

void push_arg(struct LexedFunction* lf, char* in) {
    int len = strlen(in);
    memcpy(lf->args[lf->n_args++], in, len);
}

int get_clean_function(
    struct LexedFunction* lf,
    char** input,
    int already_found) {

    int input_len = strlen(*input);
    char out[input_len];

    // Index of the end of the out buffer
    int out_ptr = 0; 

    int fname_start = -1;
    int par_end = input_len;
    int par_index = 0;
    int par_depth = 0;
    bool in_func = false;

    // Zero out the name to a value
    memset(lf->name, 0, 32);

    for (int i = 0; i < input_len ; i++) {
	char cur_char = (*input)[i];
	char prev_char = (*input)[max(1, i) - 1];
	bool is_func_start = cur_char == '(' && isalpha(prev_char);

	if (fname_start == -1 && isalpha(cur_char)) {
	    fname_start = i;
	    DEBUG("fname_start=%d", fname_start);
	}

	// This cur_char comparison is very bad but works... for now
	if (fname_start != -1 && cur_char == ' ') {
	    DEBUG("%c is not alpha", cur_char);
	    fname_start = -1;
	    continue;
	}

	// Set name if not already set
	if (is_func_start && lf->name[0] == 0) {
	    // Go backwards until we reach the start or until we reach
	    // the end of a word
	    int j;
	    for (j = i - 1; j >= 0 && isalpha((*input)[j]); j--);
	    j++;
	    for (; j < i; j++) {
		lf->name[j - fname_start] = (*input)[j];
	    }
	    lf->name[j - fname_start + 1] = '\0';
	    in_func = true;
	}
	
    }


    return 0;
}

// Should extract ONE parenthesis and return the index of the
// new parenthesis
int extract_one(char (*par)[32], char** input, int already_found) {
    int input_len = strlen(*input);
    char out[input_len];

    // Index of the end of the out buffer
    int out_ptr = 0; 

    int par_start = -1;
    int par_end = input_len;
    int par_index = 0;
    int par_depth = 0;

    for (int i = 0; i < input_len ; i++) {
	char cur_char = (*input)[i];
	char prev_char = (*input)[max(1, i) - 1];
	bool is_func = cur_char == '(' && isalpha(prev_char);

	// If we encounter a parenthesis which is NOT connected to a function
        if (cur_char == '(' && !isalpha(prev_char)) {

	    // new parenthesis was encountered before ending the previous one
	    // (if there was one being parsed), we need to reset
	    if (par_index > 0 || par_depth > 0) {
		// Move par to out!
		out[out_ptr++] = '(';
		for (int j = 0; j < par_index; j++) {
		    out[out_ptr++] = (*par)[j];
		}
	    }

            par_start = i;
	    par_index = 0;
	    par_depth++;
            continue;
        }

        if (cur_char == ')' && par_start != -1) {
	    par_depth--;
            par_end = i;
            (*par)[par_index++] = '\0';
            out[out_ptr++] = 'p';

	    // This char conversion does not work if we have more than
	    // 9 parenthesis lmao
            out[out_ptr++] = (char)(already_found + 48);


	    // Copy the rest of input into out
	    // i + 1 to skip the closing parenthesis
	    for (int j = i + 1; j < input_len; j++) {
		out[out_ptr++] = (*input)[j];
	    }
	    out[out_ptr++] = '\0';
            break;
        }

        if (par_start != -1) {
	    // We are in parenthesis
            int j = i - par_start - 1;
            (*par)[par_index++] = cur_char;
        } else {
            // copy over to out
            out[out_ptr++] = cur_char;
        }
    }

    if (par_start == -1)
	return -1;

    // TODO: Maybe realloc to free the unused space?
    memcpy(*input, out, out_ptr);
    return already_found + 1;
}

void test_new_lexer() {
    char* input = malloc(256);
    strcpy(input, "((x - y) - (2 + 1) + 6) + f(c(), NULL, 2 - 3)");
    //strcpy(input, "((6 + 4) - 4) - x");
    strcpy(input, "x + function(1,2,3)");
    struct LexedFunction* lf = malloc(sizeof(struct LexedFunction));
    int ret = get_clean_function(lf, &input, 0);
    DEBUG("lf->name = %s", lf->name);
    return;

    char par[32];
    int already_found = 0;

    for (int i = 0; i < 5; i++) {
	already_found = extract_one(&par, &input, already_found);
	if (already_found == -1) 
	    break;
	SUCCESS("input: %s", input);
	SUCCESS("p%d = %s",already_found - 1, par);
	printf("\n");
    }
}
