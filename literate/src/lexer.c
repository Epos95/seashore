#include "../../include/alok.h"
#include "../../include/eaux.h"

#include "../include/lexer.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>

extern struct AlokHandler* handler; 

// Check if `buffer` contains a valid function name for C,
// as according to GNU docs.
// TODO: Maybe add this function to a utils file or whatever.
bool is_func_name(char buffer[128], int size) {
    for (int i = 0; i < size; i++) {
        char c = buffer[i];

        // According to GNU docs these are the
	// viable characters for a c function name:
        // Any lower-case letter from a to z
        // Any upper-case letter from A to Z
        // Any digit from 0 to 9
        // The underscore character _

        // is a to z
        bool is_lowercase_alpha = c >= 0x61 && c <= 0x7a;
        // is A to Z
        bool is_uppercase_alpha = c >= 0x40 && c <= 0x5a;
        // is 0 to 9
        bool is_num = c >= 0x30 && c <= 0x39;
        // is _
        bool is_underscore = c == 0x5f;

        if (!(is_lowercase_alpha
	   || is_uppercase_alpha
	   || is_num
	   || is_underscore))
            return false;
    }

    return true;
}

bool type(char* word) {
    char* types[3] = {
        "int",
        "char",
        "float"
    };

    for (int i = 0; i < 3; i++) {
        if (strcmp(types[i], word) == 0) {
            return true;
        }
    }

    return false;
}

bool is_operator(char* op) {
    #define N 6
    char operators[N][4] = {
        "+",
        "-",
        "*",
        "/",
	"==",

    };

    for (int i = 0; i < N; i++) {
        if (strcmp(operators[i], op) == 0) {
            return true;
        }
    }

    return false;
}

// TODO: Maybe return a Word instead?
struct Word* classify_word(char* raw) {
    enum WordType wt;

    if (raw[strlen(raw) - 1] == ';') {
	raw[strlen(raw) - 1] = '\0';
    }


    int _n; // Unused actually!
    if (sscanf(raw, "%d", &_n) == 1)
        wt = IMMEDIATE;
    else if (strlen(raw) > 1) {
        int end = strlen(raw - 1);
        if ((raw[0] == '"') && (raw[end] == '"') && end > 1)
            wt = LITERAL;
        else if (is_func_name(raw, end)
                 && contains(raw, "(")
                 && contains(raw, ")"))
            wt = FUNCTION;
        else
            wt = VARIABLE;

    } else if (strlen(raw) == 1) {
        if (*raw == '(')
            wt = PARENTHESIS_BEGIN;
        else if (*raw == ')')
            wt = PARENTHESIS_END;
        else if (is_operator(raw))
            wt = OPERATOR; // Which operator tho?
        else
            wt = VARIABLE;
    } else {
        return NULL;
    }

    // Gets freed whenever we free the expression its a part of!
    // (unless we have a bad return value ofc)
    struct Word* word = malloc(sizeof(struct Word));
    word->wt = wt;
    word->data = malloc(strlen(raw));
    memcpy(word->data, raw, strlen(raw));

    return word;
}


bool contains(const char* str, const char* c) {
    int iterations = strlen(str);
    int window_size = strlen(c);

    if (window_size > (int)strlen(str)) {
        return false;
    }

    if ((window_size == (int)strlen(str)) &&
        (strncmp(str, c, window_size) == 0)) {
        return true;
    }

    for (int i = 0; i < iterations - window_size + 1; i++) {
        if (strncmp(str + sizeof(char) * i, c, window_size) == 0) {
            return true;
        }
    }

    return false;
}

void free_expression(struct Expression* ptr) {
    free(ptr->raw);
    free(ptr);

    // TODO: Free the word chain properly
}

// TODO: Maybe introduce a variable table pointer
int parse_assignment(char* line, struct Expression* expression) {

    // Return early if we have no assignment (=> Nothing for this function to do)
    bool assignment = contains(line, " = ");
    if (!assignment)
	return 0;

    const char s[2] = " ";
    char* cur = strtok(line, s);
    while (cur != NULL) {
        // This loop should only iterate aslong as the line contains a =
        // ( not actually what this code does tho )
        if (cur[0] == '=') {
            // Skip the = for when continuing parsing
            cur = strtok(NULL, s);
            break;
        }

        if (strcmp(cur, "const") == 0) {
            INFO("const!");
	    expression->declaration_flags |= F_CONST;
            goto NEXT;
        }

        // If first word and its a type:
        if (type(cur)) {
            INFO("type!");
	    expression->declaration_flags |= F_TYPE;
            goto NEXT;
        }

        // If its not any of the other things, its probably a variable name!
	INFO("Variable!");
        if (expression->declaration_flags | F_TYPE) {
            // A new variable was just declared!
            // Add to variable table
	    // If the variable already exists, error
        } else {
            // Check if its in the variable table i guess?
            // Raise a error if not
        }

	strncpy(expression->name, cur, strlen(cur));

NEXT:
        cur = strtok(NULL, s);
    }

    // TODO: return a error value for failed parsing
    return 0;
}

/*
 * Parse an *expression*, e.g "n++" in "int i = n++"
 * Can handle at most 255 tokens in one expression where each "token" is at
 * most 32 chars long.
 */
int parse(char* corpus, char ret[256][32]) {

    int n = 0;
    int char_ptr = 0;
    int buf_ptr = 0;
    char* out_buffer = malloc(sizeof(char) * 256);

    bool in_func = false;
    while (1) {
	char cur = *(corpus + char_ptr);

	bool is_op = false;
	char operators[6] = {'+', '-', '/', '*', '(', ')'};
	for (int i = 0; i < 6; i++) {
	    if (operators[i] == cur) is_op = true;
	}

	// Do special parsing if we are in a function
	if (in_func) {
	    if (cur == ')') in_func = false;
	    out_buffer[buf_ptr] = cur;
	    buf_ptr++;
	    char_ptr++;
	    continue;
	}

	if (is_op && buf_ptr > 0) {

	    // if buf is alphanumeric and cur is (, parse it as a function!
	    if (cur == '(' && is_func_name(out_buffer, buf_ptr)) {
		in_func = true;
		out_buffer[buf_ptr] = cur;
		buf_ptr++;
		char_ptr++;
		continue;
	    }

	    // "output" the buffer, since a operator means a new "word"
	    strcpy(ret[n], out_buffer);
	    n++;
	    memset(out_buffer, 0, 64);
	    buf_ptr = 0;

	    // Write the operator we encountered to our output buffer
	    out_buffer[buf_ptr] = cur;
	    buf_ptr++;

	    // Here we are assuming that the operator is only one char (cur)
	    // We therefore output the operator through the output buffer
	    strcpy(ret[n], out_buffer);
	    n++;
	    memset(out_buffer, 0, 64);
	    buf_ptr = 0;

	    // Restart parsing from the next character
	    char_ptr++;
	    continue;
	}
	
	// A opening parenthesis gets treated as its operator / token and
	// therefore gets outputted directly through the out_buffer
	if (buf_ptr == 1 && *out_buffer == '(') {
	    strcpy(ret[n], out_buffer);
	    n++;
	    memset(out_buffer, 0, 64);
	    buf_ptr = 0;
	}

	// Fucked code necessary for parsing spaces well
	if (((cur == ' ') || (cur == '\0')) ) {
	    if (buf_ptr > 0) {
		strcpy(ret[n], out_buffer);
		n++;
	    }

	    memset(out_buffer, 0, 64);
	    buf_ptr = 0;
	} else {
	    out_buffer[buf_ptr] = cur;
	    buf_ptr++;
	}

	// Exit at end of string!
	if (cur == '\0') {
	    break;
	}

	char_ptr++;
    }

    free(out_buffer);
    return n;
}

int expr(char** line_p, struct Expression* ptr) {
    int ret = 0;
    char* line = malloc(strlen(*line_p));
    strcpy(line, *line_p);

    // Gets freed by free_expression(ptr)
    struct Expression* expression = malloc(sizeof(struct Expression));

    DEBUG("full line = \"%s\"", line);
    int raw_len = strlen(line);
    // Gets freed when freeing the struct Expression*
    char* raw = malloc(sizeof(char) * raw_len);
    strcpy(raw, line);
    expression->raw = raw;
    expression->raw_len = raw_len;

    parse_assignment(line, expression);
    strcpy(line, *line_p);

    // TODO: Need to find a way to truncate line
    //       until the assignment part is gone.
    if (contains(line, "=")) {
        int i = 0;
        while (line[i] != '=') i++;
        memcpy(line, &(line[i+1]), strlen(line));
    }

    char buf[256][32];
    int n = parse(line, buf);
    
    for (int i = 0; i < n; i++) {
        struct Word* w = classify_word(buf[i]);
        if (w == NULL) {
            ERROR("Unrecognized word: \"%s\"", buf[i]);
            goto out;
        }

        //  Add the classified word to the expressions word list
        if (expression->syntax_chain == NULL) {
            expression->syntax_chain = w;
        } else {
            // Traverse list untill null, write the word to that position

            struct Word* current = expression->syntax_chain;
            while (current->next != NULL) {
                current = current->next;
            }
            current->next = w;
        }
    }

    *ptr = *expression;

out:
    // We are returning a error, free memory allocated so far
    if (ret < 0) {
	ERROR("Freeing expression!");
	free_expression(expression);
    }
    return ret;
}

// Convert a expressions word chain into reverse polish notation.
void to_rpn(struct Expression *expr)
{
    
}
