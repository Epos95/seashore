#include <stdbool.h>
#include <stddef.h>

#include "../../include/eaux.h"
#include "../include/gbuf.h"
#include "../include/x86_codegen.h"

void _write_nzero(struct GrowableBuffer* gbuf, unsigned char byte) {
    if (byte != 0)
        insert_char(gbuf, byte);
}

void write_union(struct GrowableBuffer *gbuf, void *uni, size_t size_in_bytes) {
    for (int i = 0; i < size_in_bytes; i++) {
	int shift = 8 * i;
	char byte = (*(long long*)uni & 0xff << shift) >> shift;
	DEBUG("Read byte; 0x%x @ %d", byte & 0xff, i);
	if (byte) insert_char(gbuf, byte);
    }
}


// Based on how the FullInstruction looks we create a functional instruction 
int write_instruction(struct FullInstruction instr, struct GrowableBuffer* gbuf) {
    // TODO: We can do /some/ optimizations in this function in the future, like:
    //       Using "small" ADD operations instead of ones using 
    //       a full size immediate (so 0x83 instead of 0x5).

    int idx = 0;

    bool has_prefix = instr.prefix.n != 0;
    bool has_opcode1 = instr.opcode_1 != 0;
    bool has_opcode2 = instr.opcode_2 != 0;
    bool has_modrm = instr.modrm.byte != 0;
    bool has_displacement = instr.displacement != 0;
    bool has_imm = instr.imm != 0;

    if ((!has_opcode1) || (!has_opcode2)) {
        // No opcode specified! 
        // invalid instruction.
        return -1;
    }

    if (has_prefix) {
        // Prefix is a bit of a pain to write, we need to write only the non-zero bytes
	write_union(gbuf, (void*)&instr.prefix.n, sizeof(instr.prefix));
    }

    if (has_opcode1) {
	insert_char(gbuf, instr.opcode_1);
    }

    if (has_opcode1 && has_opcode2) {
	insert_char(gbuf, instr.opcode_2);
    }

    if (has_modrm) {
	insert_char(gbuf, instr.modrm.byte);
    }

    if (has_displacement) {
	insert(gbuf, (char*)&instr.displacement, sizeof(int));
    }

    // And opcode is *something*
    if (has_imm) {
	insert(gbuf, (char*)&instr.imm, sizeof(int));
    }

    return idx;
}
