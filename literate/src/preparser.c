#include "../../include/alok.h"
#include "../../include/eaux.h"

#include "../include/preparser.h"

#include <string.h>


extern struct AlokHandler* handler;

int count_in_string(char** buf, char needle) {
    // Could be written as a while loop? (should we?)
    int count = 0;
    for (int i = 0; i < strlen(*buf); i++) {
	if ((*buf)[i] == needle)
	    count++;
    }

    return count > 0 ? count : -1;
}
    
int count_lines(char** buf) {
    return count_in_string(buf, '\n');
}


// Remove all comments and pre-processor commands from `buffer`.
// Basicaly remove all data which is "uneccessary" for program execution
// Of course we eventually handle pre-processor commands in a different way eventually
void preparser(char** buffer) {
    allocate_count_scoped_flags(char, out, strlen(*buffer), __func__, 0);
    char* copy = strdup(*buffer);
    // Keep a pointer to the copy of buffer so we can free the memory even
    // after the copy pointer has been changed by strsep.
    char* ccopy = copy; 
    int lines = count_lines(&copy);
    int consumed = 0;

    for(int i = 0; i < lines; i++) {
	char* line = strsep(&copy, "\n");
	int line_length = strlen(line);

	// For every char in the line
	// If two in a row is /, remove everything after the slashes
	// including the slashes by putting a newline
	// If the comment is on a standalone line, reject the line entirely.
	int whitespace_length = strspn(line, " ");
	for (int line_index = 0; line_index < line_length - 1; line_index++) {
	    if (line[line_index] == '/' && line[line_index + 1] == '/') {
		if (line_index == whitespace_length) {
		    // Fuck you dijkstra
		    goto REJECT_LINE; 
		} else {
		    line[line_index] = '\0';
		    break;
		}
	    }
	}

	if (line[whitespace_length] == '#') {
	    goto REJECT_LINE; 
	}

	if (strlen(line) == 0) {
	    goto REJECT_LINE; 
	}

	// strdup is necessary here because strcat FUCKING SUCKS
	char* new_line = strdup(line);

	// strcat requires __dest (new_line) to be big enough to fit dst + src + 1
	// Right now it seems like the size gets readjusted without us doing anything...
	char* new = strcat(new_line, "\n");
	out = strcat(out, new);
	free(new_line);

REJECT_LINE: // lol what
	asm("nop");
    }

    memset(*buffer, 0, strlen(*buffer));
    memcpy(*buffer, out, strlen(out));
    afree_func();
}
