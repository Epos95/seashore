#include "../../include/alok.h"
#include "../../include/eaux.h"

#include "../include/lexer.h"
#include "../include/parser2.h"
#include "../include/preparser.h"
#include "../include/codegen.h"

#include <stdio.h>
#include <string.h>

/*
struct Variable {
    char name[32];
};

struct Type {

};

struct Function {
    char name[32];


    // Arg lists seem... hard
    struct Variable* args;
    int n_args;

    struct Variable* variables;
    int n_variables;

    // List of lines of code to execute and or parse
    struct Statement* statements;
    int n_statements;


    // Do return types matter? Only the size i assume
    struct Type return_value;
    
};
union AST {
    struct Function  function;
    struct Variable  variable;
    struct Statement statement;
};

*/


enum StatementType {
    STMT_T_ASSIGNMENT,
    STMT_T_FUNCTION_CALL,
    STMT_T_KEYWORD,
};

struct Statement {
    enum StatementType type;

    
};

bool is_builtin_type(char* str) {
    // Maybe needs to remove all * from str to account for pointers to builtin types?

    // Could be moved to globals...?
    char* builtin_types[] = {
	"int",
	"char",
	"float",
	NULL
    };

    for (char** t = builtin_types; *t; t++) {
	// Maybe worth writing a length aware wrapper for strncmp?
	if (strcmp(*t, str) == 0) {
	    return true;
	}
    }
    return false;
}
    

enum StatementType what_statement(char* line) {
    // Truncate ;? Nah, assume we do that upsream instead


    // Split the line by whitespace 
    // aaaaaand suddenly we are writing a coroutine (ish)
    enum StatementType ret = -1;
    char* cur_word = strtok(line, " ");
    while (cur_word != NULL) {
	DEBUG("is_builtin_type(\"%s\") = %d", cur_word, is_builtin_type(cur_word));

	// The only time a line starts with a builtin type is if we are creating a
	// variable (and thus assigning it)
	if (is_builtin_type(cur_word)) {
	    ret = STMT_T_ASSIGNMENT;
	    goto END;
	}
	
	cur_word = strtok(NULL, " ");
    }
    

END:
    return ret;
}

extern struct AlokHandler* handler;

void test_expression() {
    struct Expression expression;
    char* str = "int i = 4;";
    //char* str = "const int i = 4 + f() - (x() + 4)";
    //char* str = "func();";
    expr(&str, &expression);


    int count = 0;
    struct Word* current = expression.syntax_chain;
    while (current != NULL) {
        count++;
        DEBUG("[%d] %s (%d)", count, current->data, current->wt);
        current = current->next;
    }

    // Test if string gets copied over
    if (strcmp(expression.raw, str) == 0) 
	SUCCESS("expression.raw == \"%s\"", expression.raw);
    else
	ERROR("expression.raw != \"%s\" (was actually \"%s\"", str, expression.raw);

    // Test things
    INFO("flags: ", expression.declaration_flags);
    printf("\t\t\t\t\t   F_TYPE  = %d\n", (expression.declaration_flags & F_TYPE) != 0);
    printf("\t\t\t\t\t   F_CONST = %d\n", (expression.declaration_flags & F_CONST) != 0);
    if ((expression.declaration_flags & F_TYPE) != 0)
	SUCCESS("Found type in declaration");
    else
	ERROR("Found no type (expected one)");

    if ((expression.declaration_flags & F_CONST) != 0)
	SUCCESS("Variable is const");
    else
	ERROR("Variable is not const");

    // Test if the right amount of Words are parsed
    if (count == 9)
	SUCCESS("Found %d Words", count);
    else
	ERROR("Expected to find 9 syntax Word");

    // Test the type hash (if there is a type)
}


void test_parsing(void) {
    char buf[256][32];
    char* str = "4 + var-6*func(5, 6, 5 + 6) + (var - var)";

    INFO("Parsing: \"%s\"", str);
    int ret = parse(str, buf);

    if (ret == 13) SUCCESS("Returned %d tokens", ret);
    else         ERROR("Found %d tokens, expected 13", ret);

    str = "4+ 5 + f()    - (2 + 1)";
    INFO("Parsing: \"%s\"", str);
    ret = parse(str, buf);

    for (int i = 0; i < ret; i++) {
        DEBUG("[%d] \"%s\"", i, buf[i]);
    }

    if (ret == 11) SUCCESS("Returned %d tokens", ret);
    else         ERROR("Found %d tokens, expected 11", ret);
}

void test_codegen(void) {
    struct Expression* expressions = malloc(sizeof(struct Expression) * 2);

    char* str = "int i = 57005"; // 0xDEAD
    expr(&str, expressions);
    int n_expressions = 1;
    if ((expressions[0].declaration_flags & F_TYPE) != 0)
	SUCCESS("Found type in declaration");
    else
	ERROR("Found no type (expected one)");

    char* new_str = "int x = j + 3;";
    // DOESNT WORK, overwrites the first one...
    expr(&new_str, &expressions[1]);
    n_expressions++;
    int count = 0;
    struct Word* current = expressions[0].syntax_chain;
    while (current != NULL) {
        count++;
        DEBUG("[%d] %s @ 0x%x", count, current->data, current->data);
        current = current->next;
    }

    current = expressions[1].syntax_chain;
    count = 0;
    while (current != NULL) {
        count++;
        DEBUG("[%d] %s @ 0x%x", count, current->data, current->data);
        current = current->next;
    }

    struct Function function = {
	.args = NULL, // No args for now
	.expressions = expressions,
	.n_expressions = n_expressions,
	.return_size = 4,
    };
    char* buffer = malloc(2048);
    struct Variable* chain = NULL;
    int r = codegen_function(buffer, &function, &chain);

    SUCCESS("Codegen returned:");
    for (int i = 0; i < r; i++) {
	printf("\t%hhx\n", buffer[i]);
    }
}

void test_scratch(void) {
    int p = 0;
    p |= 0xf1 << 8 * 0;
    p |= 0xf2 << 8 * 2;
    SUCCESS("p & 0x000000ff = 0x%x", p & 0x000000ff);
    SUCCESS("p & 0x00ff0000 = 0x%x", (p & 0x00ff0000) >> 8*2);

    for (int i = 0; i < 4; i++) {
	int shift = 8 * i;
	char b = (p & 0xff << shift) >> shift;
	if (b) SUCCESS("Found: 0x%x @ %d", b & 0xff, i);

    }
}

int main(int argc, char** argv) {
    handler = malloc(sizeof(struct AlokHandler));
    if (getenv("TESTING") != NULL) {
	INFO("Testing expression");
	test_expression();

	printf("\n");
	for (int i = 0; i < 80; i++) printf("-");
	printf("\n\n");


	INFO("Testing self written parser");
	test_parsing();

	printf("\n");
	for (int i = 0; i < 80; i++) printf("-");
	printf("\n\n");

	INFO("Testing codegen");
	test_codegen();

	printf("\n");
	for (int i = 0; i < 80; i++) printf("-");
	printf("\n\n");

	INFO("Running scratch test");
	test_scratch();

	printf("\n");
	for (int i = 0; i < 80; i++) printf("-");
	printf("\n\n");

	INFO("Testing new lexer");

	test_new_lexer();

	return 0;
    }
    
    
    char str[] = "int i = 4;";
    SUCCESS("%d", what_statement(str));
    printf("\n");
    char str2[] = "i = 4 + 5;";
    SUCCESS("%d", what_statement(str2));
    printf("\n");
    char str3[] = "int j = call_me(4) + call_me(3);";
    SUCCESS("%d", what_statement(str3));
    printf("\n");
    char str4[] = "printf(i);";
    SUCCESS("%d", what_statement(str4));
    printf("\n");

    //return 0;


    int ret = 0;
    if (argc < 2) {
	ERROR("Requires a argument file to read from");
	ret = -1;
	goto end;
    }



    int file_size = 4096;
    allocate_count_scoped_flags(char, buffer, file_size, __func__, 0);
    FILE* f = fopen(argv[1], "r");
    int n = fread(buffer, sizeof(char), file_size, f);

    int lines = count_lines(&buffer);
    SUCCESS("before:\n%s", buffer);
    INFO("buffer length: %d", strlen(buffer));
    preparser(&buffer);
    SUCCESS("after:\n%s", buffer);

    INFO("buffer length: %d", strlen(buffer));
    
end:
    afree_func();
    fclose(f);
    free(handler);
    return ret;
}
