#ifndef CODEGEN_H_
#define CODEGEN_H_

#include <stdbool.h>
#include <stddef.h>

struct Variable {
    char name[32];

    size_t size;
    int rbp_offset;

    // We still need our variable to point somewhere...

    // Altough we want to keep changes to parent scopes variables around...
    // this means child scopes will have to keep pointers to the same variables as
    // are in the parent scopes
    // ...maybe we only keep a long chain of variables around
    // since each variable has a scope builtin we can just use that
    // that means we have a variable chain and then keep a "scope" around
    // as a int in our executer or whatever. When we enter a new scope we do
    // something with the int and all variables created then have that scope.
    // we can then free all variables in a scope on exiting the scope
    int scope;
    struct Variable* next;
};

// This actually needs to keep track of the *class* of the variable
// as defined in the systemV ABI document:
// https://refspecs.linuxbase.org/elf/x86_64-abi-0.99.pdf#subsection.3.2.3
struct ArgList {
    // We can find out which register or address this comes from
    // based on its number in the arg sequence.

    char name[32];
    size_t size;

    struct ArgList* next;
};

struct Function {
    struct ArgList* args;

    struct Expression* expressions;
    int n_expressions;

    // Relevant for what we do with the return value... i think
    // We should read the systemv ABI more closesly to know
    // how much this matters
    // Basically this matters if we want to return a 128 bit value...
    // Which we dont!
    size_t return_size;
};

int codegen_function(char* code_buffer, struct Function* function, struct Variable** chain);

#endif
