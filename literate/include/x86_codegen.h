
// A full instruction /once built/ should be at most 15 bytes
// This means that our build() function should return a "short"
struct FullInstruction {
    // Since a prefix can be 0-4 bytes it needs to be a 
    // 64 bit long type (line long long) 
    // We can then use the union to address individual bits in the number :)
    // The REX part of the prefix is probably the part we want to pay the most attention to
    // since it enables 64 bit computing
    // legacy can be useful aswell but we DEFINETLY dont need VEX.
    union {
        // TODO: A bunch of things here
        unsigned long long n;
    } prefix;

    unsigned char opcode_1; // Primary opcode
    unsigned char opcode_2; // Secondary opcode

    union {
        char mod:2; // if mod == 0b11, rm is treated the same as the reg triplet
        char reg:3; // Three least significant bits of register index
        char  rm:3; 
        unsigned char byte; // The entire byte (for writing)
    } modrm;

    // 0, 1, 2, or 4 byte value... 
    unsigned int displacement;

    // 0, 1, 2, or 4 byte value... 
    unsigned int imm; // Needs to be in little endian order!
};
