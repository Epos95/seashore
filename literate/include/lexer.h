#ifndef LEXER_H
#define LEXER_H

#include <stdbool.h>

// What these flags make no sense??
// a const type is == to F_ASIGN?? NO
#define F_CONST 0b01
#define F_TYPE  0b10
#define F_ASIGN 0b11



// TODO: Make this more proper
bool contains(const char *str, const char *c);

enum WordType {
    // A number
    IMMEDIATE=1,

    // A String
    LITERAL,

    FUNCTION,
    VARIABLE,
    PARENTHESIS_BEGIN,
    PARENTHESIS_END,
    OPERATOR, // Maybe a enum member for EVERY operator?
};

struct Word {
    enum WordType wt;
    // A null-terminated string containing the data of the word
    // if the word is a immediate, it will be the number split into bytes.
    char* data; // Who allocates me?
    struct Word* next;
};

struct Expression {
    char* raw;
    int raw_len;

    // This variable contains information about the declaration itself like:
    // If its const, static whatever
    long declaration_flags;

    // Needs a way to know which type the declaration is...
    // Maybe this:
    int type_hash; // A hash lets us look up the type for when it comes to user defined types :D

    char name[32];

    // We also need some way to refer to all the Words in the expression
    // (Words are what comes after "=", so just a number in the simplest case)
    // This will be useful for actually executing the expression
    // This is also where we can do the execution and optimization of operators
    // Im thinking we do something like a "syntax chain"
    // That way we can then traverse and execute this chain
    // when converting it into machine code
    struct Word* syntax_chain;
    // Each `Word` has a next pointer!
    // We just traverse the syntax_chain like a singly linked list when necessary :D
};

void free_expression(struct Expression* ptr);
int expr(char** line_p, struct Expression* ptr);
bool is_operator(char* op);
int parse(char* corpus, char ret[256][32]);

#endif
