#ifndef GBUF_H_
#define GBUF_H_

enum GrowthStrategy {
    GS_EXP,
    GS_LIN
};

union GrowthAmount {
    double exp;
    int lin;
};

struct GrowableBuffer {
    char* ptr;
    int max_size;
    int cur_size;
    enum GrowthStrategy gs;
    union GrowthAmount ga;
};

void init_buffer(struct GrowableBuffer *buf, union GrowthAmount ga, enum GrowthStrategy gs);

void grow_buffer(struct GrowableBuffer *buf);

void insert_char(struct GrowableBuffer *buf, char c);

void insert(struct GrowableBuffer *buf, char* c, int size);

#endif
