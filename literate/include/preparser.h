#ifndef PREPARSER_H
#define PREPARSER_H

void preparser(char** buffer);
int count_in_string(char** buf, char needle);
int count_lines(char** buf);

#endif /* PREPARSER_H */
