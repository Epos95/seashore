import subprocess as sp
from pathlib import Path
from tempfile import NamedTemporaryFile
import os 

class Assertion:
    def __init__(self, assertion: str):
        self.variable_name = assertion[0]
        self.assertion = assertion
 
        if assertion[1] in ["is", "=="]:
            self.f = lambda v: v == assertion[2]
 
        if assertion[1] in ["isnt", "!="]:
            self.f = lambda v: v != assertion[2]
 
    def do(self, v) -> bool:
        return self.f(str(v))

    def __str__(self):
        return " ".join(self.assertion)

class TestFile:
    def __init__(self, buffer: str):
        # When a variable gets printed in GDB
        # its value is assigned to a 1 indexed integer
        # in the order in which its printed
        # we need to be able to translate from a index to a variable
        self.assertions = []

        # A map of names -> values to use when generating the 
        self.fixtures = {}

        self.ret = None
        self.func = None
        self.testing_function = ""

        is_func = False
        started = False
        iterator = enumerate(buffer.split("\n"))
        for line_number, line in iterator:
            # A function comes after the "ret" command
            if is_func:
                is_func = False
                self.func = line

            self.testing_function += (line + "\n")

            if "//#" not in line:
                continue

            # We are now in a line for only the tester (hopefully)
            words = line.replace("//#", "").strip().split(" ")

            if words[0] == "fix":
                var = " ".join(words[1:3])
                val = words[-1]
                self.fixtures[var] = val

            elif words[0] == "assert":
                self.assertions += [Assertion(words[1:])]
                # Add a break point and print for that variable on line {line_number+1}

            elif words[0] == "ret":
                # Store the return value to evaluate the function against
                self.ret = words[1]
                is_func = True
    def call_test_function(self):
        words = self.func.split(" ")
        ty = words[0]
        rest = " ".join(words[1:-1])

        for k, v in self.fixtures.items():
            rest = rest.replace(k, v)

        return f"{ty} ret = {rest}"

    def write_body(self, path):
        body = f"""
        {self.testing_function}
        int main() {{
            {self.call_test_function()};
        }}
        """

        with open(path, "w") as f:
            f.write(body)

        return path

    def run(self) -> bool:
        # Run the tests on the binary at the given path

        # gdb file should have been created in the constructor
        # run the test against the gdb file
        out = sp.run("gdb -q -x run_gdb.py | grep OUTPUT", shell=True, capture_output=True)
        lines = out.stdout.decode("utf-8").split("\n")

        ok = True
        for line in lines:
            if "+" not in line and line:
                print(f"\t\tTest failed: {line}")
                ok = False

        if ok:
            print("\t\tAll assertions in test passed!")
            


 
# We need different main boilerplate depending on if
# the compiler used is gcc or literate
# Literate needs a more custom harness (compiled with gcc)
# which uses the shell code directly (i think)
def compile_gcc(src_path: Path, out_path: Path) -> str | bool:
    if not src_path.exists():
        return f"{src_path} does not exist!"
    if not out_path.exists():
        return f"{out_dir} does not exist!"
 
    try:
        sp.call(f"/usr/bin/gcc -ggdb -x c {src_path} -o {out_path}", shell=True)
    except:
        # TODO: Handle exception
        pass
 
    if not out_path.exists():
        return "Compilation failed!"
 
    return True

def compile_lit(src, out):
    # First compile with literate
    # but since literate spits out binary instructions, not ELF we need to compile ANOTHER binary
    # with gcc which modifies itself to call literates code
    #
    # Except gdb wont have any idea of where to place the breakpoints if we modify the binary... ;_;
    # I think i need more of a vm solution, or something which involves stepping through the
    # two programs instead of breaking like this...
    pass
 

for test in os.listdir("tests/"):
    test = Path("tests") / test
    print(f"Running test: {test}")

    with open(test) as f:
        fc = f.read()
    test_file = TestFile(fc)
    to_compile = Path("body")

    print("\tRunning test for gcc!")
    with open("out_file", "w") as out_file:
        out_file = Path(out_file.name)

        test_file.write_body(to_compile)
        ret = compile_gcc(to_compile, out_file)
        if ret is not True:
            print(f"Compilation failed because: {ret}")
    test_file.run()

    print("\tRunning test for literate!")
    with open("out_file", "w") as out_file:
        out_file = Path(out_file.name)

        test_file.write_body(to_compile)
        ret = compile_gcc(to_compile, out_file)
        if ret is not True:
            print(f"Compilation failed because: {ret}")
    test_file.run()


if Path("out_file").exists():
    os.remove("out_file")
if Path("body").exists():
    os.remove("body")

