# Do compilation / call this script from a different script
import gdb

class Assertion:
    def __init__(self, assertion: str):
        self.variable_name = assertion[0]
        self.assertion = assertion
 
        if assertion[1] in ["is", "=="]:
            self.f = lambda v: v == assertion[2]
 
        if assertion[1] in ["isnt", "!="]:
            self.f = lambda v: v != assertion[2]
 
    def do(self, v) -> bool:
        return self.f(str(v))

    def __str__(self):
        return " ".join(self.assertion)

gdb.execute("set debuginfod enabled off")
gdb.execute("file out_file")

breakpoints = [[]]
with open("body") as f:
    lines = f.readlines()
    end_value = None
    for linenumber, line in enumerate(lines, start=1):
        if "//# ret" in line:
            end_value = line.split(" ")[-1].replace("\n", "")
            continue

        if "assert" not in line:
            breakpoints.append([])
            continue
        
        out = gdb.execute(f"b body:{linenumber+1}", to_string=True)
           
        words = list(filter(lambda s: len(s), line.replace("//#", "").strip().split(" ")))
        assertion = Assertion(words[1:])
        breakpoints[-1].append(assertion)


    if end_value:
        line_count = len(lines) - 1
        out = gdb.execute(f"b body:{line_count}")
        a = f"ret is {end_value}".split(" ")
        breakpoints.append([Assertion(a)])

        
breakpoints = list(filter(lambda x: len(x), breakpoints))
        
        
for bp in breakpoints:
    print("Breakpoints:")
    for a in bp:
        print(f"\t{a}")

gdb.execute("r")
for bp in breakpoints:
    for assertion in bp:
        print(f"Running assertion: \"{assertion}\"")
        out = gdb.execute(f"p {assertion.variable_name}", to_string=True)
        v = out.split(" ")[-1].replace("\n", "")
        if assertion.do(v):
            print(f"OUTPUT: + {assertion} was True")
        else:
            print(f"OUTPUT: - {assertion} failed (found {v}, expected {assertion.assertion[2]})")
            
    gdb.execute("c")

gdb.execute("set confirm off", from_tty=True)
gdb.execute("quit", from_tty=True)
