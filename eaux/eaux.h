
#ifndef EAUX_H_
#define EAUX_H_

// TODO: Move helpers to a separate project in seashore
//       We can then compile the helpers with options for each project
//       e.g we generate many .o or archives to match options (debugging vs no debugging or functional prints)

void print(char sym, char* file, int line_number, const char* func, const char* format, ...);

// TODO: Make easily useable under curses...

#define INFO(...)      (print('i', __FILE__, __LINE__, __func__, __VA_ARGS__))
#define SUCCESS(...)   (print('+', __FILE__, __LINE__, __func__, __VA_ARGS__))
#define ERROR(...)     (print('-', __FILE__, __LINE__, __func__, __VA_ARGS__))
#define DEBUG(...)     (print('d', __FILE__, __LINE__, __func__, __VA_ARGS__))

#define IS_ERR(v) (v < 0)

#endif // EAUX_H_
