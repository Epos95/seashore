// The kernel module

#include <linux/init.h>
#include <linux/string.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/netfilter.h>
#include <linux/netfilter_arp.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/if_ether.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <linux/kobject.h>
#include <linux/sprintf.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/kfifo.h>
#include <linux/gfp_types.h>
#include <linux/types.h>

static struct kobject *kobj;
struct nf_hook_ops hook;
unsigned int local_addr = -1;

#define PLIST_MAX_LEN 64

static DEFINE_KFIFO(plist, struct sk_buff*, PLIST_MAX_LEN);

// This mutex should protect the list of packets which are waiting to be read
DEFINE_MUTEX(m_plist);


// Could be a callback defined from user space perhaps?
// Actually this should just be a function which can come from /anywhere/ at
// compile time. This lets us choose which specialness to look for at
// compile time, which is close enough to insert time. Changing the specialnes
// would be cool but kinda difficult at runtime..?
// We might be able to change the is_special function based on user supplied
// shellcode like in i_can_change, but this requires making is_specials page
// writeable, which (in kernel) seems to also mean not making it executable
// thus preventing our function from executing itself while writing to
// is_special if they are in the same memory page.
//
// We /could/ implement our own dlopen for usage in kernel space by:
// 1. Reading the new function (from a *_show method)
// 2. Allocate a new page
// 3. Write the read shell code
// 4. Make page executable
// 5. Execute the page... :D
_Bool is_special(char* buf, size_t len) {
    // its always special for now.
    return 1;
}



static ssize_t icmpd_show(struct kobject *kobj,
    struct kobj_attribute *attr,
    char *buf) {

    // Lock early so we can be SURE there is a packet to read!
    mutex_lock(&m_plist);
    if (kfifo_len(&plist) == 0) {
	printk("icmpd: Tried to read a empty queue!\n");
	mutex_unlock(&m_plist);
	return 0;
    }

    // Write the next thing in the fifo queue to buf
    struct sk_buff* skb;
    if (!kfifo_get(&plist, &skb)) {
	// error out!
	printk("icmpd: failed to get thing from the queue ;_;\n");
	mutex_unlock(&m_plist);
	return 0;
    }
    mutex_unlock(&m_plist);

    int n = skb->len;
    memcpy(buf, skb->data, n);

    kfree_skb(skb);
    return n;
}


unsigned int hook_func(void* priv, struct sk_buff* skb,
		       const struct nf_hook_state* state)
{
    struct iphdr* ip = ip_hdr(skb);
    int protocol = ip->protocol;

    // All non-icmp packets gets accepted as normal!
    if (protocol != 1) {
	return NF_ACCEPT;
    }

    if (!is_special(skb->data, skb->len)) {
	return NF_ACCEPT;
    }

    // the networking stack will free (or decrement the reference count for) the
    // skb when we return NF_DROPPED. By cloning here i WANT the reference count
    // to be increased so dropping the packet doesnt free it. It should only
    // be freed when it has been dispatched into userspace.
    struct sk_buff* cloned_skb = skb_clone(skb, GFP_KERNEL);

    mutex_lock(&m_plist);
    printk("icmpd: %pI4 -> %pI4 - packets: %d\n",
	   &ip->saddr,
	   &ip->daddr
	   kfifo_len(&plist));
    if (!kfifo_put(&plist, cloned_skb)) {
	printk("icmpd: kfifo is overflowing! Packets are being dropped.\n");
    }
    mutex_unlock(&m_plist);

    return NF_DROP;
}

static struct kobj_attribute packets_attribute =__ATTR(packets,
						       S_IRUGO | S_IWUSR,
						       icmpd_show, NULL);

static struct attribute *packets_attrs[] = {
    &packets_attribute.attr,
    NULL,
};

static struct attribute_group attr_group = { .attrs = packets_attrs };

static int __init icmp_listener_init(void) {
    int error = 0;

    kobj = kobject_create_and_add("icmpd", kernel_kobj);
    if (!kobj) {
	error = -ENOMEM;
	goto out;
    }

    if (!(sysfs_create_group(kobj, &attr_group))) {
	printk("icmpd: stuff failed\n");
    }

    // Enumerating network stuff requires holding RTNL lock, forgetting to
    // release would prevent any network configuration from being done lmao
    printk("icmpd: Trying to lock rtnl\n");
    rtnl_lock();
    printk("icmpd: Took rtnl lock!\n");

    struct net_device *dev;
    for_each_netdev(&init_net, dev) {
        if (dev->flags & IFF_UP) {
            struct in_device* in_dev = dev->ip_ptr;

            int res = strcmp("wlp2s0f0", dev->name);
            if (res == 0) {
                local_addr = in_dev->ifa_list->ifa_address;
                printk(KERN_INFO "icmpd: IPv4 Address: %pI4\n",
		       &local_addr);

                break;
            }

        }
    }

    rtnl_unlock();

    if (local_addr == -1) {
        error = -ENODEV;
        printk("icmpd: Found no address for interface wlp0s20f3\n");
        goto out; // Error out early!
    }

    // Can we do something with these settings to make packet filtering
    // more effective?
    hook.hook = hook_func;
    hook.hooknum = NF_INET_PRE_ROUTING;
    hook.pf = NFPROTO_INET;

    error = nf_register_net_hook(&init_net, &hook);
    if (!error) {
	printk("icmpd: netfilter register success\n");
    } else {
	printk("icmpd: netfilter register fail, errno = %d\n", error);
    }

    //INIT_KFIFO(&plist);

out:
    if (error) {
	if (!kobj) kobject_put(kobj);
	// Other destructors here!
	printk("icmpd: Init failure!\n");
    } else {
	printk("icmpd: Init success!\n");
    }
    return error;
}

static void __exit icmp_listener_exit(void) {
    nf_unregister_net_hook(&init_net, &hook);
    kobject_put(kobj);
    kfifo_free(&plist);
    pr_info("icmpd: Exit success\n");
}



module_init(icmp_listener_init);
module_exit(icmp_listener_exit);

MODULE_LICENSE("GPL");
