#include <stdio.h>

typedef struct {
    int n;
} Wow;

void print_number(int num) {
    printf("Your number was %d\n", num);
}

void do_something_else() {
    printf("Now for something completely different :)\n");
}

int __return_thing(int x) {
    return x + 5;
}

int get_number(int x) {
    return __return_thing(x);
}

char get_char() {
    return 'f';
}
