#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <threads.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <poll.h>
#include <unistd.h>
#include <elf.h>
#include <errno.h>
#include <unistd.h>

#include "../include/eaux.h"
#include "read_elf.h"

// Add to a different .c file
void watch_file(char* filename) {
    // TODO: Check that file actually exists first, use access()
    if (access(filename, F_OK) == -1) {
        ERROR("File \"%s\" does not exist!", filename);
        return;
    }

    struct pollfd pfd;
    int ifd = inotify_init();
    if IS_ERR(ifd) {
        ERROR("inotify_init() failed: %s", strerror(errno));
        return;
    }

    int wd = inotify_add_watch(ifd, filename, IN_CLOSE_WRITE | IN_MODIFY | IN_CREATE | IN_DELETE);

    pfd.fd = ifd;
    pfd.events = POLLIN;

    INFO("Listening for event on %s\n", filename);
    nfds_t nfds = 1;
    int no_timeout = -1;
    int n_poll = poll(&pfd, nfds, no_timeout);

    if IS_ERR(n_poll) {
        ERROR("biiiiig error!");
    }

    // Revents is a valid pointer
    // and
    // the event is a response to POLLIN
    if (pfd.revents != 0 && pfd.revents & POLLIN) {
        INFO("there are %d events to be read...", n_poll);

        int m_size = sizeof(struct inotify_event) * n_poll;

        struct inotify_event* buf = malloc(m_size);
        struct inotify_event* ptr = &*buf;

        ssize_t bytes_read = read(pfd.fd, buf, m_size);
        if IS_ERR(bytes_read) {
            ERROR("Failed to read, error: %s", strerror(errno));
            return;
        }

        // Iter over the read events
        for (int i = 0; i < n_poll; i++) {
            struct inotify_event* event = buf;
            if (event->wd != wd) {
                ERROR("not matching wd :(");
            }

            if (event->mask & IN_IGNORED) {

                struct stat path_stat;
                stat(filename, &path_stat);

                // Check if *file* is still there, e.g not a dir,
                if (!S_ISREG(path_stat.st_mode)) {
                    ERROR("A error occured!");
                    // File was just straight up deleted...
                    // Log error accordingly
                    // Do something polling (retry), ish
                }

                // a deleting (IN_IGNORED) followed by the file still existing
                // would mean the file was recompiled...

                // Re add watch
                wd = inotify_add_watch(ifd, filename, IN_CLOSE_WRITE | IN_MODIFY | IN_CREATE | IN_DELETE);

                // Increase variable which is being watched by the thread handling shared objects

            }

            buf += sizeof(struct inotify_event);
        }

        // TODO: Make sure freeing is done properly with the error handling and stuff...
        // Also i have no clue if this freeing strategy works...
        free(ptr);
    }

    close(pfd.fd);
}


int main(int argc, char *argv[]) {

    // Spawn process calling the specific function
    // Watch for changes in the file
    // If changed:
    //   restart the process with the file

    char* filename = "./lib_shared.so";
    if (argc > 1) {
        filename = argv[1];
    }

    //watch_file(filename);
    //return 0;

    void* handle = dlopen(filename, RTLD_NOW);
    if (handle == NULL) {
        char* error = dlerror();
        ERROR("Failed to open \"%s\" due to: \n\t%s", filename, error);
        goto exit;
    } else {
        SUCCESS("Successfully opened %s (0x%p)", filename, handle);
    }

    char funcs[32][64];
    // Doesnt find any symbols :(
    int n = find_available_functions(handle, filename, funcs);

    SUCCESS("Found symbols:");
    for (int i = 0; i < n; i++) {
        printf("%d\t%s\n", i + 1, funcs[i]);
    }

    // Maybe not a great idea to block the main thread when waiting for the input...
    char choice[8];
    fgets(choice, 8, stdin);
    int index = atoi(choice);
    DEBUG("Chose index %d", index);
    char* chosen_function = funcs[index -1];
    SUCCESS("Chose symbol: %s", chosen_function);


    void (*f)() = dlsym(handle, chosen_function);
    char* error;
    if((error = dlerror()) != NULL) {
        ERROR("Got error trying to get function from shared object (%s).", error);
        goto exit;
    }

    // TODO: Use eden to find the function signature
    // And call the function with the right arguments and stuff

    SUCCESS("Output from %s: ", chosen_function);
    f(5);


    return 0;
    exit:
    ERROR("Fatal error, exiting!");
    if (handle) dlclose(handle);
    return -1;
}
