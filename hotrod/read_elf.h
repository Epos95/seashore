#ifndef READ_ELF_H_
#define READ_ELF_H_

int find_available_functions(void* handle, char* filepath, char funcs[32][64]);
int symbols_in_file(char* filepath, char symbols[32][64]);

#endif // READ_ELF_H_
