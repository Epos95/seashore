#include "read_elf.h"

#include "dlfcn.h"

#include <errno.h>
#include <stdbool.h>
#include <elf.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "../include/eaux.h"

// Check if `buffer` contains a valid, public, function name for C, as according to GNU docs.
bool _is_public_function_name(char buffer[128], int size) {
    // Ignore functions which start with underscore (treat them as private functions)
    if (buffer[0] == '_') {
        return false;
    }

    for (int i = 0; i < size; i++) {
        char c = buffer[i];

        // According to GNU docs these are the viable characters for a c function name:
        // Any lower-case letter from a to z
        // Any upper-case letter from A to Z
        // Any digit from 0 to 9
        // The underscore character _

        // is a to z
        bool is_lowercase_alpha = c >= 0x61 && c <= 0x7a;
        // is A to Z
        bool is_uppercase_alpha = c >= 0x40 && c <= 0x5a;
        // is 0 to 9
        bool is_num = c >= 0x30 && c <= 0x39;
        // is _
        bool is_underscore = c == 0x5f;

        if (!(is_lowercase_alpha || is_uppercase_alpha || is_num || is_underscore)) {
            return false;
        }
    }

    return true;
}

// Extracts valid symbols from the given file (at `filepath`).
//
// Will grab all symbols which are valid according to `_is_public_function_name` and longer than 3 chars.
// The arbitrary limit is just to disregard trash symbols, as a sideeffect of parsing things raw.
//
// Returns how many symbols were found and `-1` on error.
int symbols_in_file(char* filepath, char symbols[32][64]) {
    FILE* fd = fopen(filepath, "r");
    if (!fd) {
        ERROR("Failed to open the file!");
        return -1;
    }

    int buf_size = 4096;
    char file[buf_size];
    fread(file, sizeof(char), buf_size, fd);

    char buffer[128];
    int cur_buffer_size = 0;
    int n_symbols = 0;
    for (int i = 0; i < buf_size; i++) {
        char c = file[i];

        if (isprint(c)) {
            buffer[cur_buffer_size] = c;
            cur_buffer_size++;
        }

        // c == 0, c = \0, end of string means we want to output it (write to symbols buffer)
        // only output it if its longer than 3 since theres alot of garbage values
        // 3 is a arbitrary value which can be increased to decrease overhead when looking for
        // functions in the shared object
        if (c == 0 && cur_buffer_size > 3) {

            // Only use function names which are legal in c
            if (_is_public_function_name(buffer, cur_buffer_size)) {
                strcpy(symbols[n_symbols], buffer);
                n_symbols++;
            }

            // Zero out the buffer and reset pointer for next iteration
            memset(buffer, 0, cur_buffer_size);
            cur_buffer_size = 0;
        }
    }

    fclose(fd);
    return n_symbols;
}


// Linear search for the given section type in the section header table, return the index if found
// Otherwise return -1.
int _section_idx(int section_type, Elf64_Shdr* sh_table, int table_size) {
    for (int i = 0; i < table_size; i++) {
        if (sh_table[i].sh_type == section_type)
            return i;
    }

    return -1;
}

int _read_dynsym(char* filename, char funcs[32][64]) {
    int ret = 0;
    Elf64_Ehdr header;

    FILE* file = fopen(filename, "rb");
    if (!file) {
        // TODO: Check errno
        ERROR("Failed to open file \"%s\"! (errno: %s)", filename, strerror(errno));
        return -1;
    }

    // read the header from file
    fread(&header, sizeof(header), 1, file);
    DEBUG("offset: %lu", header.e_shoff);

    // Create section header and read into it from the file
    Elf64_Shdr* sh_table = malloc(header.e_shentsize*header.e_shnum);
    fseek(file, header.e_shoff, SEEK_SET);
    fread(sh_table, 1, header.e_shentsize*header.e_shnum, file);

    // We need to read the dynsym section, find it in the section header
    int section_index = _section_idx(SHT_DYNSYM, sh_table, header.e_shnum);
    if (section_index == -1) {
        ERROR("Could not find section for SHT_DYNSYM");
        ret = -1;
        goto EXIT_EARLY;
    }

    Elf64_Shdr dynsym_section = sh_table[section_index];

    // Read the strtab section into buffer
    section_index = _section_idx(SHT_STRTAB, sh_table, header.e_shnum);
    if (section_index == -1) {
        ERROR("Could not find section for SHT_STRTAB");
        ret = -1;
        goto EXIT_EARLY;
    }
    Elf64_Shdr str_section = sh_table[section_index];
    fseek(file, str_section.sh_offset, SEEK_SET);
    char* buffer = malloc(sizeof(char) * str_section.sh_size);
    fread(buffer, sizeof(char), str_section.sh_size, file);

    if (dynsym_section.sh_size % dynsym_section.sh_entsize != 0) {
        ERROR("non-zero remainder for sh_size / sh_entsize, something is really wrong");
        ret = -1;
        goto EXIT_LATE;
    }

    int n_entries = (int)(dynsym_section.sh_size / dynsym_section.sh_entsize);
    DEBUG("entries: %d", n_entries);

    int offset = dynsym_section.sh_offset;
    DEBUG("offset: %d", offset);

    // Go to the relevant section in the file by offsetting the file pointer
    fseek(file, offset, SEEK_SET);
    int printed_symbols = 0;
    for (int i = 0; i < n_entries; i++) {
        Elf64_Sym* sym = malloc(sizeof(Elf64_Sym));
        fread(sym, sizeof(Elf64_Sym), 1, file);

        // Skip if its not a valid pointer in the file.
        if (!sym->st_value) {
            continue;
        }

        // This could probably be done with memccpy
        char name[64];
        int idx = 0;
        for (int i = sym->st_name; (buffer[i] != '\0' && idx < 64); i++) {
            name[idx] = buffer[i];
            idx++;
        }
        name[idx] = '\0';

        INFO("%d: %s \t@ 0x%lx (0x%lx)", i, name, sym->st_value, sym->st_size);

        if (memccpy(
                    funcs[printed_symbols],
                    name,
                    '\0',
                    sizeof funcs[printed_symbols]
                    ) == NULL)
                // We overwrote the null terminator, gotta re-insert it and truncate the array
                funcs[printed_symbols][sizeof funcs[printed_symbols] - 1] = '\0';
            printed_symbols++;

        free(sym);
    }

    // finally close the file
    fclose(file);

EXIT_LATE:
    free(buffer);
EXIT_EARLY:
    free(sh_table);

    if (ret == 0)
        ret = printed_symbols;
    return ret;
}

// Finds all available functions in `filepath`.
// `handle` is expected to be a `dlopen` handle to the same file as `filepath`.
// Will use `symbols_in_file` to find symbols.
// These symbols will be tested aqainst `handle` to verify that they are real and reachable functions.
// Returns how many functions were found.
int find_available_functions(void* handle, char* filepath, char funcs[32][64]) {

    // Find all symbols in the file
    char symbols[32][64];
    int n = _read_dynsym(filepath, symbols);
    DEBUG("_read_dynsym returned: %d", n);
    //int n = symbols_in_file(filepath, symbols);

    int func_index = 0;
    for (int i = 0; i < n; i++) {
        char* string = symbols[i];

        // Dismiss the function pointer as we dont need it, just the potential error
        (void)dlsym(handle, string);

        if (dlerror() == NULL) {
            if (memccpy(
                    funcs[func_index],
                    string,
                    '\0',
                    sizeof funcs[func_index]
                    ) == NULL)
                // We overwrote the null terminator, gotta re-insert it and truncate the array
                funcs[func_index][sizeof funcs[func_index] - 1] = '\0';
            func_index++;
        }
    }

    return func_index;
}
